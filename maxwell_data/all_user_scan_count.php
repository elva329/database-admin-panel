<?php
include ('../database_config/config_maxwell.php');
$sql = "select qr_code_type, sum(user_scan_count) AS '累积扫码次数' from maxwell.qr_codes left join maxwell.user_scan on maxwell.qr_codes.id = maxwell.user_scan.qr_code_id  group by qr_code_type;";
if($result = mysqli_query($link, $sql)){
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)){
            echo "<tr>";
                echo "<td>" . $row['qr_code_type'] . "</td>";
                echo "<td>" . $row['累积扫码次数'] . "</td>";
            echo "</tr>";
        }
        // Free result set
        mysqli_free_result($result);
    } else{
        echo "No records matching your query were found.";
    }
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
 
// Close connection
mysqli_close($link);
?>