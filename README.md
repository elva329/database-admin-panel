### Introduction
This is a database panel that provided to download the campaign data of our projects. 

Currently, there are two project data that available to download:
1. Maxwell
2. Heinz

### Steps to add more project data
1. In the database_config folder, add a config file for the database of the project, named as config_[database_name]. Up the password of the database and the database name

```php
<?php
   $link = mysqli_connect("127.0.0.1", "root", "password", "database_name");
   mysqli_set_charset($link, "utf8mb4");
?>
```
2. Create a new folder under the root folder named as [project_name]_data, inside folder, create the sql query that fetch the data from the database.
3. In the Pages folder, copy the maxwell.php and rename it to [project_name].php, update the page content based on actual requirements. 

Note: All the pages should include the checkLoginStatus.php, header.php, login_user.php, footer.php and common_js.php files.