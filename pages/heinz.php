<?php include('checkLoginStatus.php');?>
<?php include('header.php');?>
<body>
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-5.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo-area">
                <img id="logo" src="../assets/img/phoceis-logo-white.png" />
                <h4 id="panel-name">Project Data Panel</h4>
            </div>
            <ul class="nav">
                <li>
                    <a href="notifications.php">
                        <i class="pe-7s-bell"></i>
                        <p>Notifications</p>
                    </a>
                </li>
                <li class="active">
                    <a href="heinz.php">
                        <i class="pe-7s-server"></i>
                        <p>Project Data</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="heinz.php">Current Project:</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">               
                              <select id="projectselector" onchange="javascript:location.href = this.value;">
                                    <option value="maxwell.php">Maxwell</option>
                                    <option value="heinz.php" selected>Heinz</option>
                             </select>  
                             <label id="select-table">Select a Data </label>
                             <select id="tableselector">
                                    <option value="section-users">Last 7 Days' Campaign Data</option>
                                    <option value="section-qr_codes">All Campaign Data</option>
                             </select>
                    </ul>

                    <?php include('login_user.php')?>
                </div>
            </div>
        </nav>
<div class="output">
  <div id="section-users" class="tables">  
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="card">
                            <div class="table users" id="users">
                                <div class="header" style ="font-size:20px; font-weight:bold; text-align:center;">
                                        Last 7 days' Campaign Data (excluding today)
                                </div>
                                <br>
                                <div class="date" style="text-align:center;">
                                    <?php date_default_timezone_set('Asia/Shanghai');?>
                                    Current Datetime: <?php echo date("F j, Y, g:i a")?>
                                    <br><br>
                                    <strong>Total Count: </strong><?php include '../heinz_data/last_7days_campaign_data_count.php'?>
                                </div>
                                
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="submit-users-7days">
                                    <thead>
                                        <th>微信昵称</th>
                                        <th>进入页面日期</th>
                                        <th>进入页面时间</th>
                                        <th>抽奖日期</th>
                                        <th>抽奖时间</th>
                                        <th>获得奖励名称</th>
                                        <th>Union ID</th>
                                    </thead>
                                    <tbody>
                                    <?php include '../heinz_data/last_7days_campaign_data.php'?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div> 
                    </div>
                </div>
             </div>
        </div>
  </div>
<div id="section-qr_codes" class="tables"> 
  <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="card">
                        <div class="table users" id="users">
                                <div class="header" style ="font-size:20px; font-weight:bold; text-align:center;">
                                        All Campaign Data
                                </div>
                                <br />
                                <div class="date" style="text-align:center;">
                                        Since 2019-07-06 till Now
                                        <br><br>
                                    <strong>Total Count: </strong><?php include '../heinz_data/total_count.php'?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <strong>New Data Today: </strong><?php include '../heinz_data/count_of_today.php'?>
                                </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="all-submit-users">
                                    <thead>
                                    <th>微信昵称</th>
                                        <th>进入页面日期</th>
                                        <th>进入页面时间</th>
                                        <th>抽奖日期</th>
                                        <th>抽奖时间</th>
                                        <th>获得奖励名称</th>
                                        <th>Union ID</th>
                                    </thead>
                                    <tbody>
                                    <?php include '../heinz_data/all_campaign_data.php'?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
  </div>
  <?php include('footer.php')?>
    </div>
</div>
<?php include('common_js.php');?>
</body>
</html>
<script>
    $(function() {
  $('#tableselector').change(function(){
    $('.tables').hide();
    $('#' + $(this).val()).show();
  });
});
</script>

<script>
    var DefaultTable = document.getElementById('submit-users-7days');
    new TableExport(DefaultTable, {
        headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
        footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
        formats: ["xlsx"],         // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
        filename: '亨氏活动报告',                             // (id, String), filename for the downloaded file, (default: 'id')
        bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: false)
        position: 'top',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
        ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
        ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
        ignoreCSS: '.tableexport-ignore',           // (selector, selector[]), selector(s) to exclude cells from the exported file(s) (default: '.tableexport-ignore')
        emptyCSS: '.tableexport-empty',             // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s) (default: '.tableexport-empty')
        trimWhitespace: true,                       // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: true)
        RTL: false,                                 // (Boolean), set direction of the worksheet to right-to-left (default: false)
        sheetname: '上周用户抽奖数据'                             // (id, String), sheet name for the exported spreadsheet, (default: 'id')
    });
</script>

<script>
    var DefaultTable = document.getElementById('all-submit-users');
    new TableExport(DefaultTable, {
        headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
        footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
        formats: ["xlsx"],         // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
        filename: '亨氏活动报告',                             // (id, String), filename for the downloaded file, (default: 'id')
        bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: false)
        position: 'top',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
        ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
        ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
        ignoreCSS: '.tableexport-ignore',           // (selector, selector[]), selector(s) to exclude cells from the exported file(s) (default: '.tableexport-ignore')
        emptyCSS: '.tableexport-empty',             // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s) (default: '.tableexport-empty')
        trimWhitespace: true,                       // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: true)
        RTL: false,                                 // (Boolean), set direction of the worksheet to right-to-left (default: false)
        sheetname: '全部用户抽奖数据'                             // (id, String), sheet name for the exported spreadsheet, (default: 'id')
    });
</script>
