<?php include('checkLoginStatus.php');?>
<?php include('header.php');?>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-5.jpg">
            <!--   you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple" -->
            <div class="sidebar-wrapper">
                <div class="logo-area">
                    <img id="logo" src="../assets/img/phoceis-logo-white.png" />
                    <h4 id="panel-name">Project Data Panel</h4>
                </div>

                <ul class="nav">
                    <li class="active">
                        <a href="notifications.php">
                            <i class="pe-7s-bell"></i>
                            <p>Notifications</p>
                        </a>
                    </li>
                    <li>
                        <a href="maxwell.php">
                            <i class="pe-7s-server"></i>
                            <p>Project Data</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <a class="navbar-brand" href="#">Notifications</a>
                    </div>
                    <div class="collapse navbar-collapse">
                     
                    <?php include('login_user.php')?>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="card">
                        <div class="header">
                        </div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul>
                                        <li class="notification">
                                            <span>【2019-07-08】Data for Project Heinz is available for downloading! </span><img id="new" src="../assets/img/new.png" />
                                        </li>
                                        <li class="notification">
                                            <span>【2019-05-07】Data for Project Maxwell is available for downloading! </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <?php include('footer.php')?>
        </div>
    </div>
</body>

<!--   Core JS Files   -->
<script src="../assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>

</html>