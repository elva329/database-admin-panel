<ul class="nav navbar-nav navbar-right">
                            <li>
                                <?php if (isset($_SESSION['success'])) : ?>
                                    <div class="error success" >
                                        <h3>
                                            <?php 
                                                echo $_SESSION['success']; 
                                                unset($_SESSION['success']);
                                            ?>
                                        </h3>
                                    </div>
		                        <?php endif ?>
                                <!-- logged in user information -->
                                <div class="profile_info">  
                                    <div class="current-user" style="margin-top:20px;">		
                                        <div>
                                            <?php  if (isset($_SESSION['user'])) : ?>
                                                <strong><?php echo $_SESSION['user']['username']; ?></strong>
                                                    <a style="text-decoration:underline" id="logout" href="../login.php?logout='1'" style="color: red;">Logout</a>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>