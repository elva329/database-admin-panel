<?php
include ('../database_config/config_heinz.php');
$sql = "select nickname, enter_date,enter_time,submit_date, submit_time,reward_type, union_id from users, reward_type where users.reward_type_id = reward_type.id AND submit_date between date_sub(SUBDATE(NOW(),1),INTERVAL 1 WEEK) and SUBDATE(NOW(),1) ORDER BY submit_date DESC;";
if($result = mysqli_query($link, $sql)){
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)){
            echo "<tr>";
                echo "<td>" . $row['nickname'] . "</td>";
                echo "<td>" . $row['enter_date'] . "</td>";
                echo "<td>" . $row['enter_time'] . "</td>";
                echo "<td>" . $row['submit_date'] . "</td>";
                echo "<td>" . $row['submit_time'] . "</td>";
                echo "<td>" . $row['reward_type'] . "</td>";
                echo "<td>" . $row['union_id'] . "</td>";
            echo "</tr>";
        }
        mysqli_free_result($result);
    } else{
        echo "No records matching your query were found.";
    }
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
// Close connection
mysqli_close($link);
?>