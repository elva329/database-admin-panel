<?php
include ('../database_config/config_maxwell.php');
$sql = "select username, tel, submit_date, qr_code_type, coupon_name, sequence_no from maxwell.qr_codes, maxwell.coupons where maxwell.qr_codes.coupon_id = maxwell.coupons.id AND qr_code_used =1 ORDER BY submit_date DESC;";
if($result = mysqli_query($link, $sql)){
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_array($result)){
            echo "<tr>";
                echo "<td>" . $row['username'] . "</td>";
                echo "<td>" . $row['tel'] . "</td>";
                echo "<td>" . $row['submit_date'] . "</td>";
                echo "<td>" . $row['coupon_name'] . "</td>";
                echo "<td>" . $row['qr_code_type'] . "</td>";
                echo "<td>" . $row['sequence_no'] . "</td>";
            echo "</tr>";
        }
        mysqli_free_result($result);
    } else{
        echo "No records matching your query were found.";
    }
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
 
// Close connection
mysqli_close($link);
?>