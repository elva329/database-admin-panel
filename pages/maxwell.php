<?php include('checkLoginStatus.php');?>
<?php include('header.php');?>

<body>
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-5.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo-area">
                <img id="logo" src="../assets/img/phoceis-logo-white.png" />
                <h4 id="panel-name">Project Data Panel</h4>
            </div>
            <ul class="nav">
                <li>
                    <a href="notifications.php">
                        <i class="pe-7s-bell"></i>
                        <p>Notifications</p>
                    </a>
                </li>
                <li class="active">
                    <a href="maxwell.php">
                        <i class="pe-7s-server"></i>
                        <p>Project Data</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="maxwell.php">Current Project:</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">               
                              <select id="projectselector" onchange="javascript:location.href = this.value;">
                                    <option value="maxwell.php" selected>Maxwell</option>
                                    <option value="heinz.php">Heinz</option>
                             </select>  
                             <label id="select-table">Select a Data </label>
                             <select id="tableselector">
                                    <option value="section-today">Today's Campaign Data</option>
                                    <option value="section-users">Last 7 Days' Campaign Data</option>
                                    <option value="section-qr_codes">All Campaign Data</option>
                                    <!-- <option value="section-unsubmitted">Unsubmitted User Data</option> -->
                                    <option value="section-coupon_codes">Last 7 Days' User Scan Count</option>
                                    <option value="section-coupons">All User Scan Count</option>
                             </select>
                    </ul>

                   <?php include('login_user.php')?>
                </div>
            </div>
        </nav>

<div class="output">
<div id="section-today" class="tables today">  
      <div class="content"></div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="card">
                            <div class="table today" id="today">
                                <div class="header" style ="font-size:20px; font-weight:bold; text-align:center;">
                                        Today's Campaign Data
                                </div>
                                <br>
                                <div class="date" style="text-align:center;">
                                    <?php date_default_timezone_set('Asia/Shanghai');?>
                                    Current Datetime: <?php echo date("F j, Y, g:i a")?>
                                    <br><br>
                                    <strong>Total Count: </strong><?php include '../maxwell_data/count_of_today.php'?>
                                </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="submit-today">
                                    <thead>
                                        <th>姓名</th>
                                    	<th>电话</th>
                                    	<th>提交时间</th>
                                        <th>兑换奖励</th>
                                        <th>SKU类型</th>
                                        <th>序列号</th>
                                    </thead>
                                    <tbody>
                                    <?php include '../maxwell_data/today_campaign_data.php'?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>  
                    </div>
                </div>
             </div>
        </div>
  </div>
  <div id="section-users" class="tables users">  
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="card">
                            <div class="table users" id="users">
                                <div class="header" style ="font-size:20px; font-weight:bold; text-align:center;">
                                        Last 7 days' Campaign Data (excluding today)
                                </div>
                                <br>
                                <div class="date" style="text-align:center;">
                                    <?php date_default_timezone_set('Asia/Shanghai');?>
                                    Current Datetime: <?php echo date("F j, Y, g:i a")?>
                                    <br><br>
                                    <strong>Total Count: </strong><?php include '../maxwell_data/total_count_7days.php'?>
                                </div>
                                
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="submit-users">
                                    <thead>
                                        <th>姓名</th>
                                    	<th>电话</th>
                                    	<th>提交时间</th>
                                        <th>兑换奖励</th>
                                        <th>SKU类型</th>
                                        <th>序列号</th>
                                    </thead>
                                    <tbody>
                                    <?php include '../maxwell_data/last_7days_campaign_data.php'?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>  
                    </div>
                </div>
             </div>
        </div>
  </div>
  <div id="section-coupon_codes" class="tables coupon-codes"> 
      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="card">
                            <div class="header" style ="font-size:20px; font-weight:bold; text-align:center;">
                                        Last 7 Days' User Scan Count
                            </div>
                            <br>
                                <div class="date" style="text-align:center;">
                                    <?php date_default_timezone_set('Asia/Shanghai');?>
                                    Current Datetime: <?php echo date("F j, Y, g:i a")?>
                                </div>
                            <div class="table coupon_codes" id="coupon_codes">
                                <div class="content table-responsive table-full-width">
                                    <table class="table table-hover table-striped" id="user-scan-count">
                                        <thead>
                                            <th>SKU名称</th>
                                            <th>累计扫码次数</th>
                                        </thead>
                                        <tbody>
                                        <?php include '../maxwell_data/last_7days_user_scan_count.php'?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
        </div>
  </div>
  <div id="section-qr_codes" class="tables qr-codes"> 
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="card">
                        <div class="table qr-codes" id="qr-codes">
                                <div class="header" style ="font-size:20px; font-weight:bold; text-align:center;">
                                        All Campaign Data
                                </div>
                                <br />
                                <div class="date" style="text-align:center;">
                                        Since 2019-04-30 till Now
                                        <br><br>
                                    <strong>Total Count: </strong><?php include '../maxwell_data/total_count.php'?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <strong>New Data Today: </strong><?php include '../maxwell_data/count_of_today.php'?>
                                </div>
                            <div class="content table-responsive table-full-width">
                                    <table class="table table-hover table-striped" id="all-submit-users">
                                        <thead>
                                            <th>姓名</th>
                                            <th>电话</th>
                                            <th>提交时间</th>                             
                                            <th>兑换奖励</th>
                                            <th>SKU类型</th>
                                        </thead>
                                        <tbody>
                                        <?php include '../maxwell_data/all_campaign_data.php'?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
         </div>
    </div>
</div>
<!-- <div id="section-unsubmittd" class="tables unsubmittd"> 
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="card">
                        <div class="table unsubmittd" id="unsubmittd">
                                <div class="header" style ="font-size:20px; font-weight:bold; text-align:center;">
                                        Unsubmitted User Data
                                </div>
                                <br />
                                <div class="date" style="text-align:center;">
                                        Since 2019-04-30 till Now
                                        <br><br>
                                    <strong>Total Count: </strong><?php include '../maxwell_data/unsubmitted_total_count.php'?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            <div class="content table-responsive table-full-width">
                                    <table class="table table-hover table-striped" id="unsubmittd">
                                        <thead>
                                            <th>昵称</th>
                                            <th>打开链接时间</th>                             
                                            <th>奖励名称</th>
                                            <th>SKU类型</th>
                                            <th>序列号</th>
                                        </thead>
                                        <tbody>
                                        <?php include '../maxwell_data/unsubmitted_campaign_data.php'?>
                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
         </div>
    </div>
</div> -->

<div id="section-coupons" class="tables coupons"> 
    <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="card">
                        <div class="header" style ="font-size:20px; font-weight:bold; text-align:center;">
                                        All User Scan Count
                            </div>
                            <br />
                                <div class="date" style="text-align:center;">
                                        Since 2019-05-21 till Now
                                </div>
                            <div class="table coupon_codes" id="coupons">
                            <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped" id="all-user-scan">
                                    <thead>
                                        <th>SKU名称</th>
                                    	<th>累计扫码次数</th>
                                    </thead>
                                    <tbody>
                                    <?php include '../maxwell_data/all_user_scan_count.php'?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
  </div>
</div>
       <?php include('footer.php')?>
    </div>
</div>
    <?php include('common_js.php');?>

</body>
  
</html>
<script>
    $(function() {
    $('.today').show();
    $('#section-users, #section-coupon_codes, #section-qr_codes, #section-coupons').hide();
});
$('#tableselector').change(function(){
    $('.tables').hide();
    $('#' + $(this).val()).show();
  });
</script>
<script>
    var DefaultTable = document.getElementById('submit-today');
    new TableExport(DefaultTable, {
        headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
        footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
        formats: ["xlsx","csv"],         // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
        filename: '麦斯威尔活动报告',                             // (id, String), filename for the downloaded file, (default: 'id')
        bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: false)
        position: 'top',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
        ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
        ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
        ignoreCSS: '.tableexport-ignore',           // (selector, selector[]), selector(s) to exclude cells from the exported file(s) (default: '.tableexport-ignore')
        emptyCSS: '.tableexport-empty',             // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s) (default: '.tableexport-empty')
        trimWhitespace: true,                       // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: true)
        RTL: false,                                 // (Boolean), set direction of the worksheet to right-to-left (default: false)
        sheetname: '今日用户提交数据'                             // (id, String), sheet name for the exported spreadsheet, (default: 'id')
    });
</script>
<script>
    var DefaultTable = document.getElementById('submit-users');
    new TableExport(DefaultTable, {
        headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
        footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
        formats: ["xlsx","csv"],         // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
        filename: '麦斯威尔活动报告',                             // (id, String), filename for the downloaded file, (default: 'id')
        bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: false)
        position: 'top',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
        ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
        ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
        ignoreCSS: '.tableexport-ignore',           // (selector, selector[]), selector(s) to exclude cells from the exported file(s) (default: '.tableexport-ignore')
        emptyCSS: '.tableexport-empty',             // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s) (default: '.tableexport-empty')
        trimWhitespace: true,                       // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: true)
        RTL: false,                                 // (Boolean), set direction of the worksheet to right-to-left (default: false)
        sheetname: '上周用户提交数据'                             // (id, String), sheet name for the exported spreadsheet, (default: 'id')
    });
</script>

<script>
    var DefaultTable = document.getElementById('user-scan-count');
    new TableExport(DefaultTable, {
        headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
        footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
        formats: ["xlsx","csv"],         // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
        filename: '麦斯威尔活动报告',                             // (id, String), filename for the downloaded file, (default: 'id')
        bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: false)
        position: 'top',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
        ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
        ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
        ignoreCSS: '.tableexport-ignore',           // (selector, selector[]), selector(s) to exclude cells from the exported file(s) (default: '.tableexport-ignore')
        emptyCSS: '.tableexport-empty',             // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s) (default: '.tableexport-empty')
        trimWhitespace: true,                       // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: true)
        RTL: false,                                 // (Boolean), set direction of the worksheet to right-to-left (default: false)
        sheetname: '上周扫码次数'                             // (id, String), sheet name for the exported spreadsheet, (default: 'id')
    });
</script>

<script>
    var DefaultTable = document.getElementById('all-submit-users');
    new TableExport(DefaultTable, {
        headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
        footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
        formats: ["xlsx","csv"],         // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
        filename: '麦斯威尔活动报告',                             // (id, String), filename for the downloaded file, (default: 'id')
        bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: false)
        position: 'top',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
        ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
        ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
        ignoreCSS: '.tableexport-ignore',           // (selector, selector[]), selector(s) to exclude cells from the exported file(s) (default: '.tableexport-ignore')
        emptyCSS: '.tableexport-empty',             // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s) (default: '.tableexport-empty')
        trimWhitespace: true,                       // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: true)
        RTL: false,                                 // (Boolean), set direction of the worksheet to right-to-left (default: false)
        sheetname: '全部提交数据'                             // (id, String), sheet name for the exported spreadsheet, (default: 'id')
    });
</script>

<script>
    var DefaultTable = document.getElementById('all-user-scan');
    new TableExport(DefaultTable, {
        headers: true,                              // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
        footers: true,                              // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
        formats: ["xlsx","csv"],         // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
        filename: '麦斯威尔活动报告',                             // (id, String), filename for the downloaded file, (default: 'id')
        bootstrap: true,                           // (Boolean), style buttons using bootstrap, (default: false)
        position: 'top',                         // (top, bottom), position of the caption element relative to table, (default: 'bottom')
        ignoreRows: null,                           // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
        ignoreCols: null,                           // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
        ignoreCSS: '.tableexport-ignore',           // (selector, selector[]), selector(s) to exclude cells from the exported file(s) (default: '.tableexport-ignore')
        emptyCSS: '.tableexport-empty',             // (selector, selector[]), selector(s) to replace cells with an empty string in the exported file(s) (default: '.tableexport-empty')
        trimWhitespace: true,                       // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: true)
        RTL: false,                                 // (Boolean), set direction of the worksheet to right-to-left (default: false)
        sheetname: '全部累积扫码次数'                             // (id, String), sheet name for the exported spreadsheet, (default: 'id')
    });
</script>